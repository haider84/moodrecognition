# moodRecognition

The MoodBox is used to extract privacy-based features for each audio recording session. It is noted that the privacy-based features are extracted for 16 recordings from 4 participants. For regression analysis, we averaged the privacy-based feature (extracted for each voice segment) over an audio recording. The regression analysis is performed using linear and random forest regression methods in leave one (recording) out cross-validation setting using MATLAB. We used the Concordance correlation Coefficient (CCC) and Pearson Correlation Coefficient (r) for evaluation purposes. The results are reported below in the table. It is noted that the linear regression (0.3376) provides better r than random forest (0.2555). However random forest provides the best CCC (0.2371) for joy prediction. For predicting the wakefulness score, the random forest provides better results than linear regression, as shown in Table C&E below.

Table C&E

Regression Method CCC r

Joy Linear 0.1129 0.3376

Joy Random Forest 0.2371 0.2555

Wakefulness Linear -0.0457 -0.1231

Wakefulness Random Forest 0.0230 0.0256

