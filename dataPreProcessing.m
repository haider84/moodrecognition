close all
clear all
clc


meta= readtable('CognitionEntries.csv', 'PreserveVariableNames',true);

id=meta{1:end,1:1};
date=meta{1:end,3:3};
%d=meta{1:end,3:3};
user_id=meta{1:end,4:4};
L_J=meta{1:end,5:5};
L_W=meta{1:end,6:6};


load('UserDeviceConfigurations.mat')
id_ud=UserDeviceConfigurations{2:end,1:1};
user_id_ud=UserDeviceConfigurations{2:end,2:2};
location_id_ud=UserDeviceConfigurations{2:end,3:3};

k=1;
for i=1:1:length(user_id_ud)
    
   for j=1:1:length(user_id)
      
       
       tf=strcmp(user_id_ud{i}, user_id{j});
       
       if(tf==1)
          
           location_f{k,:}=location_id_ud{i};
           user_id_f{k,:}=user_id_ud{i};
           date_f{k,:}=date{j};
           data_J(k,:)=L_J(j);
           data_W(k,:)=L_W(j);
           id_f{k,:}=id_ud{i};
           k=k+1;
       end
       
       
       
   end
    
end



