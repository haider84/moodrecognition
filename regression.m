close all
clear all
clc

load('moodboxClean.mat')

for i=1:1:length(data_af)
    

   if(length(mean(data_af{i}))<2) 
   temp(i,:)=data_af{i};
   end
   if(length(mean(data_af{i}))>2)
   temp(i,:)=mean(data_af{i});
   end    
    
end


clear k j

j=1:1:length(a);

labels=c;  

for k=1:1:length(a)

%dataADR=[nADR ADR dADR dnADR];
 %trainADR=[age' gender' nADR dnADR temporal];age' gender'
trainADR=[ temp];
trainLabels=labels';
trainADR(k,:)=[];
trainLabels(k)=[]; 
 %valADR=[age(k)' gender(k)' nADR(k,:) dnADR(k,:) temporal(k,:)];age(k)' gender(k)' 
valADR=[temp(k,:)];


valLabels=labels(k);
t = templateSVM('Standardize',1);
tf= templateTree('MinLeafSize',1);

MdlQuadratic = fitlm(trainADR,trainLabels);
% MdlQuadratic = fitrsvm(trainADR,trainLabels,'Standardize',true,'KernelFunction','Linear','KernelScale','auto','Solver','SMO','BoxConstraint',10000000);
% MdlQuadratic = fitrgp(trainADR,trainLabels','KernelFunction','squaredexponential');
%  MdlQuadratic = fitrtree(trainADR,trainLabels,'MinLeafSize',5);
% MdlQuadratic =fitrensemble(trainADR,trainLabels,'NumLearningCycles',10,'Learners',tf);

% figure;
% view(MdlQuadratic,'Mode','graph');
% view(MdlQuadratic);
% MdlQuadratic = fitcknn(trainADR,trainLabels);
% MdlQuadratic = TreeBagger(10,trainADR,trainLabels,'MinLeafSize',hp);

pre= predict(MdlQuadratic,valADR);
% pre=str2num(cell2mat(pre));
l(k)=pre;

 
end
ll=l;

[R,P] = corrcoef(labels',ll);

out=[labels' ;ll]';
CCC1 = f_CCC(out,0.05);
CCC=CCC1{1,1}.est

R=CCC1{1, 1}.pearsonCorrCoeff  

