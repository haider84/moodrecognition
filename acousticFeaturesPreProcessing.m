close all
clear all
clc
load('features.mat')
load('userMappingCE.mat')

meta= readtable('metadata.csv', 'PreserveVariableNames',true,'Delimiter',',');

id_mb=meta{1:end,1:1};
date_mb=meta{1:end,2:2};
location_mb=meta{1:end,3:3};

for i=1:1:length(date_f)
date_f{i,:}=date_f{i}(1:10);

end

for i=1:1:length(date_mb)
data_mb{i,:}=date_mb{i}(1:10);

end
temp=unique(location_mb);
k=1;
for i=1:1:length(location_f)
   
    for j=1:1:length(temp)
        
        tf=strcmp(location_f{i}, temp{j});
       
       if(tf==1)
       a{k,:}=location_f{i};
       b{k,:}=date_f{i};
       joy(k,:)=data_J(i);
       wak(k,:)=data_W(i);
       k=k+1;
       
       end
        
    end
    
end


save('labels','wak','joy','location','date')